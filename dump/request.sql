UPDATE wp_options
SET option_value = replace(option_value, 'http://bedrock.local/wp', 'http://wp-<projectName>.local/wp')
WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE wp_posts
SET guid = replace(guid, 'http://bedrock.local', 'http://wp-<projectName>.local');

UPDATE wp_postmeta
SET meta_value = replace(meta_value, 'http://bedrock.local', 'http://wp-<projectName>.local');

UPDATE wp_posts
SET post_content = replace(post_content, 'http://bedrock.local', 'http://wp-<projectName>.local');
