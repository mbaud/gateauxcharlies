<?php
/**
 * The template for displaying the header
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

use App\Utils\Path;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="l-site">
    <header class="l-header" role="banner">
        <div class="o-container">
            <div class="flex justify-center items-center py-4">
                <?php get_template_part(Path::HEADER_PATH.'/header-menu'); ?>
            </div>
        </div>
    </header>

    <div class="l-content">
