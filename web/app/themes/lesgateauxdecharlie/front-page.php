<?php
/**
 * The template for displaying the last posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#home-page-display
 */

use App\Utils\Path;
use App\Utils\Parts\Breadcrumb;
use App\Utils\Parts\Sidebar;
use App\Utils\Parts\Social;
use VeronQ\ACFU;


$creation      = get_field('bloc-creation') ?: [];
$creation_link = $creation['link'];

$logo = get_field('logo-home') ?: [];

$cheat      = get_field('bloc-cheat') ?: [];
$cheat_link = $cheat['link'];

$label = get_field('label-home');

get_header();
?>


    <section class="py-10">
        <div class="w-full flex flex-row justify-between">
            <div class="w-1/3">
                <h2 class="text-white text-center text-5xl">
                    <?php if (!empty($creation_link)) {
                        new ACFU\Link($creation_link, '', ['class' => ' z-50  px-8 py-8 shadow bg-secondary-100']);
                    } ?>
                </h2>
                <span class="shadow ">
                <?php if (($creation['image'])) {
                    new ACFU\Image($creation['image'], 'large', ['class' => ' h-full w-full  rounded-full shadow  ']);
                } ?>
                </span>
            </div>
            <div class="w-1/2 flex justify-center absolute right-25 ">
                <?php if (!empty($logo)): ?>
                    <img class=" z-1 bg-white w-full rounded-full " src="<?php echo esc_url($logo['url']); ?>"
                         alt="<?php echo esc_attr($logo['alt']); ?>"/>
                <?php endif; ?>
            </div>
            <div class="w-1/3 ">
                <h2 class="text-white text-center  text-5xl">
                    <?php if (!empty($cheat_link)) {
                        new ACFU\Link($cheat_link, '', ['class' => ' z-50  px-8 py-8 bg-secondary-100 shadow']);
                    } ?>
                </h2>
                <span class="shadow">
                <?php if (($cheat['image'])) {
                    new ACFU\Image($cheat['image'], 'large', ['class' => ' h-full w-full  rounded-full shadow  ']);
                } ?>
                </span>
            </div>
        </div>
        <div class=" o-container">
            <div class="flex flex-col text-5xl text-center pt-48">
                <?= $label; ?>
                <div class="o-container">
        <span class="text-xl flex justify-center ">
	            <?php Social::social(false); ?>
            </span>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
