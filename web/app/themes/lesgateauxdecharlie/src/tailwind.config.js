const theme = require('./tailwind-config/theme')
const variants = require('./tailwind-config/variants')
const variant = require('./tailwind-config/variant')
const corePlugins = require('./tailwind-config/core-plugins')

module.exports = {
    theme,
    variants,
    plugins: [
        variant,
    ],
    corePlugins,
}
