module.exports = {
    screens: {
        /*
        'sm': '576px',
        'md': '768px',
        'lg': '1024px',
        'xl': '1440px',
        */
    },
    colors: {
        // @see: https://maketintsandshades.com
        transparent: 'transparent',
        current: 'currentColor',

        black: '#000',
        white: '#fff',

        gray: {
            100: '#f7fafc',
            200: '#edf2f7',
            300: '#e2e8f0',
            400: '#cbd5e0',
            500: '#a0aec0',
            600: '#718096',
            700: '#4a5568',
            800: '#2d3748',
            900: '#1a202c',
        },
        secondary: {
            100: '#008593',
            200: '#87745B',
            300: '#B71C5B',
            400: '#D4AB79',
        },
        primary: {
            100: '#d2ebe9',
            200: '#a5d7d4',
            300: '#78c3be',
            400: '#4bafa9',
            500: '#1e9b93',
            600: '#1b8c84',
            700: '#156d67',
            800: '#0f4e4a',
            900: '#092e2c',
        },
    },
    maxWidth: {
        container: '1192px',
        halfContainer: '596px',
        full: '100%',
    },
    zIndex: {
        '-1': -1,
        0: 0,
        1: 1,
        2: 2,
    },
    extend: {
        inset: {
            4: '1rem',
            25: '25%',
            50: '50%',
            100: '100%',
        },
        fontFamily: {
            core: [
                'Athena',
                'sans-serif',
            ],
            heading: [
                'Athena',
                'sans-serif',
            ],
            icon: [
                'Icomoon',
            ],
        },
    },
}
