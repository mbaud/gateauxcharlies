const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')
const purgecss = require('@fullhuman/postcss-purgecss')

const purgecssConfig = {
    content: [
        '../**/*.php',
    ],
    whitelist: [],
    defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
}

const isProduction = process.env.npm_lifecycle_script === 'encore production'

module.exports = {
    plugins: [
        tailwindcss(),
        autoprefixer({
            grid: 'autoplace'
        }),
        ...(isProduction ? [purgecss(purgecssConfig)] : [])
    ]
}
