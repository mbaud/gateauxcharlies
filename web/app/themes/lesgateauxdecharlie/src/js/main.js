import { HandleScroll } from './components/'

document.addEventListener('DOMContentLoaded', () => {
    HandleScroll()
})
