export default () => {
    const links = document.querySelectorAll('a[href^="#"]');

    [...links].forEach(link => {
        link.addEventListener('click', (e) => {
            e.preventDefault()
            const hash = link.getAttribute('href')
            const target = document.querySelector(hash)
            if (!target) return

            target.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
            history.pushState(null, null, hash)
        })
    })
}
