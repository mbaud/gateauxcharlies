const path = require('path')
const Encore = require('@symfony/webpack-encore')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const isProduction = Encore.isProduction()

const assetsPath = path.resolve('./')
const outputPath = path.resolve('../dist')
const publicPath = path.resolve('/app/themes/lesgateauxdecharlie/dist')

const jsPath = path.join(assetsPath, './js')
const cssPath = path.join(assetsPath, './css')
const fontsPath = path.join(assetsPath, './fonts')
const imagesPath = path.join(assetsPath, './images')

Encore
	.cleanupOutputBeforeBuild()
	.setPublicPath(publicPath)
	.setOutputPath(outputPath)
	.disableSingleRuntimeChunk()

	.addAliases({
		'~css': cssPath,
		'~js': jsPath,
		'~fonts': fontsPath,
		'~images': imagesPath
	})

	.addEntry('main',
		[
			path.join(jsPath, '/main.js'),
			path.join(cssPath, '/main.scss'),
		]
	)
	.addStyleEntry('admin',
		[
			path.join(cssPath, '/admin.scss'),
		]
	)
	.enableSassLoader()
	.enablePostCssLoader()
	.enableSourceMaps(!isProduction)
	.enableVersioning(isProduction)

	.configureFilenames({
		js: './js/[name].min.js',
		css: './css/[name].min.css'
	})

	.addPlugin(
		new BundleAnalyzerPlugin({
			openAnalyzer: false
		})
	)
;

const config = Encore.getWebpackConfig()

config.watchOptions = {
	poll: true,
	ignored: /node_modules/
}

module.exports = config
