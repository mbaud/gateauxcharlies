<?php
/**
 * The main template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

use App\Utils\Path;
use App\Utils\Parts\Breadcrumb;
use App\Utils\Parts\Sidebar;

get_header();
?>

    <div class="o-container">
        <div class="mb-4">
            <?php Breadcrumb::get([get_the_ID()]); ?>
        </div>

        <div class="o-grid">
            <main class="o-col-9">

                <?php
                if (have_posts()) :
                    while (have_posts()) {
                        the_post();
                        get_template_part(Path::CONTENT_PATH.'/content');
                    }
                else :
                    get_template_part(Path::CONTENT_PATH.'/content', 'none');
                endif;
                ?>

            </main>

            <aside class="o-col-3" role="complementary">
                <?php Sidebar::get(); ?>
            </aside>
        </div>
    </div>

<?php
get_footer();
