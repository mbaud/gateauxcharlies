<?php

require_once __DIR__ . '/App/Polyfill/ArrayPolyfill.php';

$functions = [
    \App\Config\Setup::class,
    \App\Config\Cleanup::class,
    \App\Config\Plugins::class,
    \App\Config\Assets::class,

    \App\Register\TranslationsRegister::class,
    \App\Register\ImageSizesRegister::class,
    \App\Register\MenusRegister::class,
    \App\Register\PostTypesRegister::class,
    \App\Register\SidebarRegister::class,
    \App\Register\TaxonomiesRegister::class,

    \App\Views\FrontViews::class,
    \App\Views\AdminViews::class,
    \App\Views\LoginViews::class,
];

foreach ($functions as $function) {
    new $function();
}
