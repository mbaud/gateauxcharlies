<?php

declare(strict_types=1);

namespace App\Register;

use App\Config\Config;
use App\Traits\DefaultValuesTrait;

/**
 * Class SidebarRegister
 * @package App\Register
 */
class SidebarRegister
{
    use DefaultValuesTrait;
    /**
     * @var array
     */
    protected $sidebars;

    /**
     * SidebarRegister constructor.
     */
    public function __construct()
    {
        $this->sidebars = Config::getConfigValue('sidebars');
        $this->setDefaultValues($this->getDefaultValues());
        add_action('widgets_init', [$this, 'registerSidebars']);
    }

    /**
     * @return array
     */
    protected function getDefaultValues(): array
    {
        return [
            'description'   => __('Ajouter de nouveaux widgets.', 'gpc'),
            'before_widget' => '<section class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        ];
    }

    /**
     * Register all sidebars.
     */
    public function registerSidebars(): void
    {
        foreach ($this->sidebars as $key => $value) {
            register_sidebar(
                array_merge(
                    [
                        'id' => $key,
                    ],
                    array_replace_recursive($this->getDefaultValues(), $value)
                )
            );
        }
    }
}
