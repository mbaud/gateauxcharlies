<?php

declare(strict_types=1);

namespace App\Register;

use App\Config\Config;

/**
 * Class TranslationsRegister
 * @package App\Register
 */
class TranslationsRegister
{
    /**
     * @var array
     */
    protected $translations;

    /**
     * TranslationsRegister constructor.
     */
    public function __construct()
    {
        $this->translations = Config::getConfigValue('translations');

        if (function_exists('pll_register_string')) {
            $this->registerTranslationsWithPolylang($this->translations);
        }
    }

    /**
     * @param array $translations
     */
    protected function registerTranslationsWithPolylang(array $translations): void
    {
        foreach ($translations as $context => $strings) {
            foreach ($strings as $name => $string) {
                pll_register_string($name, $string, $context);
            }
        }
    }
}
