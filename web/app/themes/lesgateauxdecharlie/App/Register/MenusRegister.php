<?php

declare(strict_types=1);

namespace App\Register;

use App\Config\Config;
use App\Utils\Localization\Trans;

/**
 * Class MenusRegister
 * @package App\Register
 */
class MenusRegister
{
    /**
     * @var array
     */
    protected $menus;

    /**
     * MenusRegister constructor.
     */
    public function __construct()
    {
        $this->menus = Config::getConfigValue('menus');
        add_action('after_setup_theme', [$this, 'registerMenus']);
    }

    /**
     * Register all menus.
     */
    public function registerMenus(): void
    {
        register_nav_menus(
            array_map(static function ($item) {
                return Trans::__($item);
            }, $this->menus)
        );
    }
}
