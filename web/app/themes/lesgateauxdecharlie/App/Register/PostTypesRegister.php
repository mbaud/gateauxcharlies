<?php

declare(strict_types=1);

namespace App\Register;

use App\Config\Config;

/**
 * Class PostTypesRegister
 * @package App\Register
 */
class PostTypesRegister
{
    /**
     * @var array
     */
    protected $post_types;

    /**
     * PostTypesRegister constructor.
     */
    public function __construct()
    {
        $this->post_types = Config::getConfigValue('post_types');
        add_action('init', [$this, 'registerPostTypes']);
    }

    /**
     * Register all Post Types.
     */
    public function registerPostTypes(): void
    {
        foreach ($this->post_types as $key => $value) {
            register_post_type($key, $value);
        }
    }
}
