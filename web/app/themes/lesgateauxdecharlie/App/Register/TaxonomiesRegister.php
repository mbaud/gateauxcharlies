<?php

declare(strict_types=1);

namespace App\Register;

use App\Config\Config;

/**
 * Class TaxonomiesRegister
 * @package App\Register
 */
class TaxonomiesRegister
{
    /**
     * @var array
     */
    protected $taxonomies;

    /**
     * TaxonomiesRegister constructor.
     */
    public function __construct()
    {
        $this->taxonomies = Config::getConfigValue('taxonomies');
        add_action('init', [$this, 'registerTaxonomies']);
    }

    /**
     * Register all Taxonomies.
     */
    public function registerTaxonomies(): void
    {
        foreach ($this->taxonomies as $key => $value) {
            register_taxonomy($key, $value['relation'], $value['args']);
        }
    }
}
