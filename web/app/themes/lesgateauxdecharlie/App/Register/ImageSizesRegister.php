<?php

declare(strict_types=1);

namespace App\Register;

use App\Config\Config;

/**
 * Class ImageSizesRegister
 * @package App\Register
 */
class ImageSizesRegister
{
    /**
     * @var array
     */
    protected $sizes;

    /**
     * ImageSizesRegister constructor.
     */
    public function __construct()
    {
        $this->sizes = Config::getConfigValue('image_sizes');
        add_action('after_setup_theme', [$this, 'addImageSizes']);
        add_filter('image_size_names_choose', [$this, 'filterImageNames']);
    }

    /**
     * Register new custom image sizes.
     */
    public function addImageSizes(): void
    {
        foreach ($this->sizes as $key => $value) {
            [$width, $height] = $value['size'];
            $crop = $value['crop'];
            add_image_size($key, $width, $height, $crop);
        }
    }

    /**
     * Filters the names and labels of the default image sizes.
     *
     * @param array $size_names
     *
     * @return array
     */
    public function filterImageNames(array $size_names): array
    {
        foreach ($this->sizes as $key => $value) {
            if (array_key_exists('name', $value)) {
                $size_names[$key] = $value['name'];
            }
        }

        return $size_names;
    }
}
