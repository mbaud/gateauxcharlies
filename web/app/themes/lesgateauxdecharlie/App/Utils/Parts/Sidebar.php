<?php

declare(strict_types=1);

namespace App\Utils\Parts;

/**
 * Class Sidebar
 * @package App\Utils
 */
class Sidebar
{
    protected const DEFAULT_SIDEBAR_NAME = 'sidebar-default';

    /**
     * @param  string  $name
     */
    public static function get(string $name = self::DEFAULT_SIDEBAR_NAME): void
    {
        if ( ! is_active_sidebar($name)) {
            $name = self::DEFAULT_SIDEBAR_NAME;
        }

        dynamic_sidebar($name);
    }
}
