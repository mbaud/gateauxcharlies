<?php

declare( strict_types=1 );

namespace App\Utils\Parts;

use App\Utils\Localization\Trans;

/**
 * Class Social
 * @package App\Utils\Parts
 */
class Social {
	/**
	 * Display social link
	 *
	 * @param bool $is_vertical
	 */

	public static function social( $is_vertical = false ) {
		$socials = get_field( 'site-social', 'option' );
		if ( $socials ): ?>
			<div class="Social<?= $is_vertical ? ' Social--vertical' : ''; ?>">
				<?php foreach ( $socials as $social ) : ?>
					<a href="<?= $social['link']; ?>"
					   class="Social-link"
					   target="_blank"
					   rel="noopener noreferrer"
					   title="<?php printf( __( 'Suivez-nous sur %s !', 'Wordpress' ), $social['title']['label'] ); ?>">
						<i class="Icon-<?= $social['title']['value']; ?>"></i>
					</a>

				<?php endforeach ?>
			</div>
		<?php endif;

	}
}

