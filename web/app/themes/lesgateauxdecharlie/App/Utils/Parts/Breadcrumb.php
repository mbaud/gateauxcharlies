<?php

declare(strict_types=1);

namespace App\Utils\Parts;

use App\Utils\Localization\Trans;

/**
 * Class Breadcrumb
 * @package App\Utils\Parts
 */
class Breadcrumb
{
    /**
     * @param  array  $posts
     */
    public static function get(array $posts): void
    {
        if ($posts[0] !== get_option('page_on_front')) {
            array_unshift($posts, get_option('page_on_front'));
        }
        ?>
        <ol vocab="https://schema.org/" typeof="BreadcrumbList" class="flex">

            <?php foreach ($posts as $key => $post) :
                $data = self::getData($post);
                if ($data === null) {
                    continue;
                }
                ?>

                <li property="itemListElement" typeof="ListItem" class="Breadcrumb-item">
                    <a property="item" typeof="WebPage" href="<?= $data['permalink']; ?>">
                        <span property="name">
                            <?= $data['title']; ?>
                        </span>
                    </a>
                    <meta property="position" content="<?= $key + 1; ?>"/>
                </li>

            <?php endforeach; ?>

        </ol>
        <?php
    }

    /**
     * @param $post
     *
     * @return array|null
     */
    protected static function getData($post): ?array
    {
        $title     = get_the_title($post);
        $permalink = get_the_permalink($post);

        if (post_type_exists($post)) {
            if ($post !== 'post' && ! get_post_type_object($post)->has_archive) {
                return null;
            }
            $title     = get_post_type_object($post)->label;
            $permalink = get_post_type_archive_link($post);
        }

        if (in_array($post, ['search', 'category', 'tag'])) {
            $permalink = (isset($_SERVER['HTTPS']) ? "https" : "http")
                         ."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }

        switch ($post) {
            case 'search':
                $title = sprintf(
                    Trans::__('Recherche « %s »'),
                    get_search_query()
                );
                break;
            case 'category':
                $title = sprintf(
                    Trans::__('Catégorie « %s »'),
                    single_cat_title('', false)
                );
                break;
            case 'tag':
                $title = sprintf(
                    Trans::__('Tag « %s »'),
                    single_tag_title('', false)
                );
                break;
        }

        return [
            'permalink' => $permalink,
            'title'     => $title,
        ];
    }
}
