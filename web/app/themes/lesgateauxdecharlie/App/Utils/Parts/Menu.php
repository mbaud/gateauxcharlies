<?php

declare(strict_types=1);

namespace App\Utils\Parts;

/**
 * Class Menu
 *
 * @package App\Utils
 */
class Menu
{
    /**
     * @param  string  $location
     * @param  array   $params
     *
     * @return false|string|void
     */
    public static function get(string $location, array $params = [])
    {
        if (has_nav_menu($location)) {
            return wp_nav_menu($params);
        }

        return null;
    }
}
