<?php

declare(strict_types=1);

namespace App\Utils\Localization;

use App\Config\Setup;

/**
 * Class Trans
 * @package App\Utils\Localization
 */
class Trans
{
    /**
     * @param string $text
     * @param string $domain
     *
     * @return string|void
     */
    public static function __(string $text, string $domain = Setup::DOMAIN_NAME): string
    {
        return function_exists('pll__')
            ? pll__($text)
            : __($text, $domain);
    }

    /**
     * @param string $text
     * @param string $domain
     */
    public static function _e(string $text, string $domain = Setup::DOMAIN_NAME): void
    {
        function_exists('pll_e')
            ? pll_e($text)
            : _e($text, $domain);
    }
}
