<?php

declare(strict_types=1);

namespace App\Utils\Types;

/**
 * Class ArrayUtils
 * @package App\Utils\Types
 */
class ArrayUtils
{
    /**
     * @param array  $arr
     * @param string $key
     *
     * @return array
     */
    public static function remove(array $arr, string $key)
    {
        unset($arr[$key]);

        return $arr;
    }
}
