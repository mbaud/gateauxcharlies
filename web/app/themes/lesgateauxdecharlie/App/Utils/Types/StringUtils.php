<?php

declare(strict_types=1);

namespace App\Utils\Types;

/**
 * Class StringUtils
 * @package App\Utils\Types
 */
class StringUtils
{
    /**
     * Truncate a given text.
     *
     * @param string $excerpt
     * @param int    $length
     *
     * @return bool|string|string[]|null
     */
    public static function truncate(string $excerpt, int $length)
    {
        if (strlen(utf8_decode($excerpt)) > $length) {
            $excerpt = preg_replace(' ([.*?])', '', $excerpt);
            $excerpt = strip_shortcodes($excerpt);
            $excerpt = strip_tags($excerpt);
            $excerpt = substr($excerpt, 0, $length);
            $excerpt = substr($excerpt, 0, strripos($excerpt, ' '));
            $excerpt = $excerpt.'...';
        }

        return $excerpt;
    }
}
