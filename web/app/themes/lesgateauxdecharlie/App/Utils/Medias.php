<?php

declare(strict_types=1);

namespace App\Utils;

/**
 * Class Medias
 * @package App\Utils
 */
class Medias
{
    /**
     * Get png filepath by name.
     *
     * @param string $name
     *
     * @return string
     */
    public static function getPng(string $name): string
    {
        return Path::getStatic().'/png/'.$name.'.png';
    }

    /**
     * Get jpg filepath by name.
     *
     * @param string $name
     *
     * @return string
     */
    public static function getJpg(string $name): string
    {
        return Path::getStatic().'/jpg/'.$name.'.jpg';
    }

    /**
     * Include an svg file by name.
     *
     * @param string $name
     */
    public static function incSvg(string $name): void
    {
        include get_template_directory().'/static/svg/'.$name.'.svg';
    }
}
