<?php

declare(strict_types=1);

namespace App\Utils;

/**
 * Class Path
 * @package App\Utils
 */
class Path
{
    public const CUSTOM_PATH = 'template-parts/custom';
    public const CUSTOM_PATH_INDEX = self::CUSTOM_PATH.'/index';
    public const CUSTOM_PATH_HOME = self::CUSTOM_PATH.'/home';
    public const CUSTOM_PATH_SINGLE = self::CUSTOM_PATH.'/single';

    public const ARTICLE_PATH = 'template-parts/article';
    public const CONTENT_PATH = 'template-parts/content';
    public const HEADER_PATH = 'template-parts/header';

    /**
     * Get App directory path.
     *
     * @return string
     */
    public static function getApp(): string
    {
        return get_template_directory_uri().'/App';
    }

    /**
     * Get dist directory path.
     *
     * @return string
     */
    public static function getDist(): string
    {
        return get_template_directory_uri().'/dist';
    }

    /**
     * Get static directory path.
     *
     * @return string
     */
    public static function getStatic(): string
    {
        return get_template_directory_uri().'/static';
    }
}
