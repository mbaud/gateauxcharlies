<?php

declare(strict_types=1);

namespace App\Utils;

use WP_Query;

/**
 * Class Query
 * @package App\Utils
 */
class Query
{
    /**
     * @param string $post_type
     * @param int    $number
     * @param array  $params
     *
     * @return WP_Query
     */
    public static function baseQuery(string $post_type, int $number = -1, array $params = []): WP_Query
    {
        $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
        $args  = [
            'post_type'      => $post_type,
            'posts_per_page' => $number,
            'post_status'    => 'publish',
            'paged'          => $paged,
        ];
        if ($params) {
            $args = array_merge($args, $params);
        }
        $the_query = new WP_Query($args);

        return $the_query;
    }
}
