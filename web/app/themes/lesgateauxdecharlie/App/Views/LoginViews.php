<?php

declare(strict_types=1);

namespace App\Views;

use App\Utils\Medias;

/**
 * Class LoginViews
 * @package App\Views
 */
class LoginViews
{
    protected const AUTHOR_URI = 'https://www.asdoria.com/';

    /**
     * Login constructor.
     */
    public function __construct()
    {
        add_action('login_head', [$this, 'setLogoBackground']);
        add_filter('login_headerurl', [$this, 'filterLogoUrl']);
    }

    /**
     * Use Asdoria logo instead of WordPress logo.
     */
    public function setLogoBackground(): void
    {
        echo '<style>h1 a {background-image: url('.Medias::getPng('logo-asdoria').') !important; }</style>';
    }

    /**
     * Redirect to Asdoria website.
     *
     * @return string
     */
    public function filterLogoUrl(): string
    {
        return self::AUTHOR_URI;
    }
}
