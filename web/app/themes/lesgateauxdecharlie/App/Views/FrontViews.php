<?php

declare(strict_types=1);

namespace App\Views;

use stdClass;
use WP_POST;

/**
 * Class FrontViews
 * @package App\Views
 */
class FrontViews
{
    /**
     * Admin constructor.
     */
    public function __construct()
    {
        add_filter('nav_menu_item_id', '__return_false');
        add_filter('nav_menu_css_class', [$this, 'filterNavAttributes'], 100, 1);
        add_filter('nav_menu_link_attributes', [$this, 'addNavLinkAttributes'], 1, 3);
    }

    /**
     * Clear nav attributes.
     *
     * @param array|string $classes
     *
     * @return array|string
     */
    public function filterNavAttributes($classes)
    {
        if (in_array('current-menu-item', $classes, true)) {
            $classes[] = 'is-current';
        }

        return is_array($classes)
            ? array_intersect($classes, ['is-current'])
            : '';
    }

    /**
     * Add a new link class argument in wp_nav_menu.
     *
     * @param array    $atts
     * @param WP_POST  $item
     * @param stdClass $args
     *
     * @return array
     */
    public function addNavLinkAttributes(array $atts, WP_POST $item, stdClass $args): array
    {
        if (property_exists($args, 'link_class')) {
            $atts['class'] = $args->link_class;
        }

        if ($item->current && property_exists($args, 'link_class_active')) {
            $atts['class'] .= ' '.$args->link_class_active;
        }

        return $atts;
    }
}
