<?php

declare(strict_types=1);

namespace App\Views;

use App\Utils\Path;
use WP_Admin_Bar;

/**
 * Class AdminViews
 * @package App\Views
 */
class AdminViews
{
    /**
     * Admin constructor.
     */
    public function __construct()
    {
        add_filter('admin_footer_text', '__return_empty_string');
	    add_action('admin_print_styles', [$this, 'enqueueAdminStyle'], 11);
	    add_action('admin_bar_menu', [$this, 'removeWPLogo'], 999);
        add_action('admin_init', [$this, 'removePanels']);
    }

    /**
     * Enqueue admin custom CSS.
     */
    public function enqueueAdminStyle(): void
    {
        wp_enqueue_style('admin-css', Path::getDist().'/css/admin.min.css');
    }

    /**
     * Remove WordPress Logo.
     *
     * @param WP_Admin_Bar $wp_admin_bar
     */
    public function removeWPLogo(WP_Admin_Bar $wp_admin_bar): void
    {
        $wp_admin_bar->remove_node('wp-logo');
    }

    /**
     * Remove some dashboard panels.
     */
    public function removePanels(): void
    {
        remove_action('welcome_panel', 'wp_welcome_panel');
        remove_action('try_gutenberg_panel', 'wp_try_gutenberg_panel');
        remove_meta_box('dashboard_primary', 'dashboard', 'side');
    }
}
