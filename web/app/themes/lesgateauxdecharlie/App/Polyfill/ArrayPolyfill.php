<?php

declare(strict_types=1);

/**
 * @see https://www.php.net/manual/en/function.array-key-first.php
 * @version 7.3.0
 */
if ( ! function_exists('array_key_first')) {
    function array_key_first(array $arr)
    {
        foreach ($arr as $key => $unused) {
            return $key;
        }

        return null;
    }
}
