<?php

declare(strict_types=1);

namespace App\Config;

/**
 * Class Plugins
 * @package App\Config
 */
class Plugins
{
    protected const GOOGLE_MAPS_API_KEY = 'AIzaSyChANyXQKGAi3RRnf0aJ_ZWbstMTgsSV4A';

    /**
     * Plugins constructor.
     */
    public function __construct()
    {
        $this->addOptionsPage();
        $this->setGoogleMapsApiKey();
    }

    /**
     * Add an option page to the admin panels.
     */
    protected function addOptionsPage(): void
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page();
        }
    }

    /**
     * Set Google Maps api Key.
     */
    protected function setGoogleMapsApiKey(): void
    {
        if (function_exists('acf_update_setting')) {
            acf_update_setting('google_api_key', self::GOOGLE_MAPS_API_KEY);
        }
    }
}
