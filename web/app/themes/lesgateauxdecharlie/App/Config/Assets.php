<?php

declare(strict_types=1);

namespace App\Config;

use App\Utils\Path;

/**
 * Class Assets
 *
 * @package App\Register
 */
class Assets
{
    protected const CURRENT_VERSION = 1;

    protected const VERSIONED_ASSETS = [
        'asdoria-css',
        'asdoria-js',
    ];

    /**
     * Assets constructor.
     */
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueStyles']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_action('wp_footer', [$this, 'deregisterScripts']);
        add_filter('style_loader_src', [$this, 'assetsVersioning'], PHP_INT_MAX, 2);
        add_filter('script_loader_src', [$this, 'assetsVersioning'], PHP_INT_MAX, 2);
    }

    /**
     * Enqueue styles.
     */
    public function enqueueStyles(): void
    {
        wp_enqueue_style('asdoria-css', Path::getDist().'/css/main.min.css');
    }

    /**
     * Enqueue scripts.
     */
    public function enqueueScripts(): void
    {
        wp_enqueue_script('asdoria-js', Path::getDist().'/js/main.min.js', [], null, true);
    }

    /**
     * Deregister scripts.
     */
    public function deregisterScripts(): void
    {
        if ( ! is_admin()) {
            wp_deregister_script('jquery');
        }

        wp_deregister_script('wp-embed');
    }

    /**
     * Filters an enqueued style’s fully-qualified URL.
     *
     * @param string $src
     * @param string $handle
     *
     * @return string
     */
    public function assetsVersioning(string $src, string $handle): string
    {
        if (strpos($src, 'ver=')) {
            $src = remove_query_arg('ver', $src);
        }

        if (in_array($handle, self::VERSIONED_ASSETS, true)) {
            $src .= '?ver='.self::CURRENT_VERSION;
        }

        return $src;
    }
}
