<?php

declare(strict_types=1);

namespace App\Config;

use Symfony\Component\Yaml\Yaml;

/**
 * Class Config
 * @package App\Config
 */
class Config
{
    /**
     * @var self
     */
    protected static $instance;

    /**
     * @var array
     */
    protected $config;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->config = $this->setConfig(Yaml::parseFile(__DIR__.'/../Resources/config.yaml')['config']);
    }

    /**
     * @return Config
     */
    public static function getInstance(): Config
    {
        if (empty(self::$instance)) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function getConfigValue(string $key)
    {
        return self::getInstance()->getConfig()[$key] ?? [];
    }

    /**
     * Set config.
     *
     * @param array $config
     *
     * @return array
     */
    protected function setConfig(array $config): array
    {
        return $this->handleImports($config);
    }

    /**
     * Handle yaml resources import.
     *
     * @param array $arr
     *
     * @return array
     */
    public function handleImports(array $arr): array
    {
        if ( ! empty($arr['imports'])) {
            foreach ($arr['imports'] as $import) {
                if (empty($import['resource'])) {
                    continue;
                }
                $content = Yaml::parseFile(__DIR__.'/../Resources/'.$import['resource']);
                $key     = array_key_first($content);
                if ( ! empty($content[$key]['imports'])) {
                    $content[$key] = self::handleImports($content[$key]);
                }
                $arr[$key] = $content[$key];
            }
        }
        unset($arr['imports']);

        return $arr;
    }
}
