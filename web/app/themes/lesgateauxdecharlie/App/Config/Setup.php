<?php

declare(strict_types=1);

namespace App\Config;

/**
 * Class Setup
 * @package App\Config
 */
class Setup
{
    public const DOMAIN_NAME = 'PROJECTNAME';

    /**
     * Setup constructor.
     */
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'initTheme']);
        add_action('after_setup_theme', [$this, 'setContentWidth'], 0);
        add_action('after_switch_theme', 'flush_rewrite_rules');
    }

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     */
    public function initTheme(): void
    {
        load_theme_textdomain(self::DOMAIN_NAME, get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        // Let WordPress manage the document title.
        add_theme_support('title-tag');

        // Enable support for Post Thumbnails on posts and pages.
        add_theme_support('post-thumbnails');

        // Registers an image size for the post thumbnail.
        set_post_thumbnail_size(1200, 630, true);

        // Switch default core markup for search form, comment form, and comments to output valid HTML5.
        add_theme_support(
            'html5',
            [
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            ]
        );

        // Add support for core custom logo.
        add_theme_support(
            'custom-logo',
            [
                'height'      => 80,
                'width'       => 160,
                'flex-width'  => false,
                'flex-height' => true,
            ]
        );

        // Add support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Add support for full and wide align images.
        add_theme_support('align-wide');

        // Add support for editor styles.
        add_theme_support('editor-styles');

        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');

        // Add support for excerpt in pages.
        add_post_type_support('page', 'excerpt');
    }

    /**
     * Set the content width in pixels, based on the theme's design and stylesheet.
     */
    public function setContentWidth(): void
    {
        $GLOBALS['content_width'] = 760;
    }
}
