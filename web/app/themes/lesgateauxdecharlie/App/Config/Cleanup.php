<?php

declare(strict_types=1);

namespace App\Config;

/**
 * Class Cleanup
 * @package App\Config
 */
class Cleanup
{
    /**
     * Cleanup constructor.
     */
    public function __construct()
    {
        $this->cleanupHeader();
        $this->cleanupEmojis();

        add_filter('xmlrpc_enabled', '__return_false');
    }

    /**
     * Cleanup Header.
     */
    protected function cleanupHeader(): void
    {
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'start_post_rel_link', 10);
        remove_action('wp_head', 'parent_post_rel_link', 10);
        remove_action('wp_head', 'adjacent_posts_rel_link', 10);
        remove_action('wp_head', 'wp_shortlink_wp_head', 10);
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);
    }

    /**
     * Cleanup emojis.
     */
    protected function cleanupEmojis(): void
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        add_filter('emoji_svg_url', '__return_false');
    }
}
