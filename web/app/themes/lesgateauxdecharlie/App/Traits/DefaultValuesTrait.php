<?php

declare(strict_types=1);

namespace App\Traits;

/**
 * Trait DefaultValuesTrait
 * @package App\Traits
 */
trait DefaultValuesTrait
{
    /**
     * @var array
     */
    protected $defaultValues;

    /**
     * @param mixed $defaultValues
     */
    public function setDefaultValues($defaultValues): void
    {
        $this->defaultValues = $defaultValues;
    }

    /**
     * @param array  $values
     * @param string $key
     *
     * @return mixed
     */
    public function getValue(array $values, string $key)
    {
        return $values[$key] ?? $this->defaultValues[$key];
    }
}
