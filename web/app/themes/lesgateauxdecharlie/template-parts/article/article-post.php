<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('mb-4'); ?>>
    <header class="border-b text-primary-500">
        <h2>
            <a href="<?php the_permalink(); ?>" rel="bookmark">
                <?php the_title(); ?>
            </a>
        </h2>
    </header>

    <div class="mt-2">
        <?php the_excerpt(); ?>
    </div>
</article>
