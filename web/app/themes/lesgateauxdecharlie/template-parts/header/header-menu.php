<?php

use App\Utils\Parts\Menu;

$menu_params = [
    'theme_location'    => 'header-menu',
    'container'         => false,
    'echo'              => false,
    'link_class'        => 'mx-2',
    'link_class_active' => 'underline',
];
?>

<nav class="text-3xl">
    <?= strip_tags(Menu::get('header-menu', $menu_params), '<a>'); ?>
</nav>
