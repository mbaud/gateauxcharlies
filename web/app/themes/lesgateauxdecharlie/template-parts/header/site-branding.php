<?php

use App\Utils\Localization\Trans;

?>
<a href="<?= home_url() ?>"
   title="<?php Trans::_e('Retour à l\'accueil'); ?>">
    <img src="<?= esc_url(wp_get_attachment_image_url(get_theme_mod('custom_logo'), '160x')); ?>"
         alt="<?php Trans::_e('Logo du groupe'); ?>"
         data-no-lazy="1">
</a>
