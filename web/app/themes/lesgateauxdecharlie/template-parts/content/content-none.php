<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

use App\Utils\Localization\Trans;

?>

<section class="no-results not-found">
    <h1 class="text-5xl">
        <?php Trans::_e('Aucun résultat', 'asdoria'); ?>
    </h1>
</section>
