<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article>
    <header class="border-b text-primary-500">
        <h1>
            <?php the_title(); ?>
        </h1>
    </header>

    <div class="l-markdown">
        <?php the_content(); ?>
    </div>
</article>
