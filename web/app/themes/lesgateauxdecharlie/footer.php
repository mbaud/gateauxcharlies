<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

use App\Utils\Localization\Trans;
use App\Utils\Medias;
use App\Utils\Parts\Menu;

$menu_params = [
	'theme_location' => 'footer-menu',
	'container'      => false,
	'echo'           => false,
	'before'         => '<span class="l-footer-line__item">',
	'after'          => '</span>',
	'link_class'     => 'mx-1',
];
?>

</div>

<footer class="l-footer">
    <div class="o-container">
        <div class="flex justify-between flew-wrap py-2 text-gray-200 text-sm">
            <!----  Display Asdoria Footer Copyright --->
            <div>
		        <?php
		        printf( Trans::__( '© %d %s - Tous droits réservés - Création' ),
			        current_time( 'Y' ),
			        get_bloginfo( 'name' ) )
		        ?>
                <a href=""><img class="inline-block "
                                                        src="<?php echo Medias::getPng( '' ); ?>">
                </a>
            </div>
            <div class="flex">
				<?= strip_tags( Menu::get( 'footer-menu', $menu_params ), '<span><a>' ); ?>
            </div>
        </div>
    </div>


</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
